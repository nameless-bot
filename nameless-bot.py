# Huge respects to Shinobiwan. He is a very helpful dude, and was always helpful when I needed him to be.

import socket
from die import roll
from time import strftime

#now it's easier to modify the bots nickname and username,
#just change these strings and it will update in the rest
#of the code where %s substitution is used

botnick = "botty1224"
botuser = "botty"
bottrigger = "_"
botchannel = "#botschool"
network = 'localhost'
port = 6664
irc = socket.socket ( socket.AF_INET, socket.SOCK_STREAM )
irc.connect ( ( network, port ) )
irc.send ( bytes ( 'USER %s %s %s :Python IRC\r\n'% ( botuser, botuser, botuser ), "UTF-8" ) )
irc.send ( bytes ( 'NICK %s\r\n'% (botnick), "UTF-8" ) )
irc.send ( bytes ( 'JOIN %s\r\n'% ( botchannel ), "UTF-8" ) )
irc.send ( bytes ( 'PRIVMSG %s :Hello World.\r\n'% ( botchannel ), "UTF-8" ) )



def say(msg):
    irc.send ( bytes ( 'PRIVMSG %s :%s%s'% ( botchannel, msg, '\r\n' ), "UTF-8" ) )
def nameless():
    say ("                                             /$$                             ")
    say ("                                            | $$                             ")
    say (" /$$$$$$$   /$$$$$$  /$$$$$$/$$$$   /$$$$$$ | $$  /$$$$$$   /$$$$$$$ /$$$$$$$")
    say ("| $$__  $$ |____  $$| $$_  $$_  $$ /$$__  $$| $$ /$$__  $$ /$$_____//$$_____/")
    say ("| $$  \ $$  /$$$$$$$| $$ \ $$ \ $$| $$$$$$$$| $$| $$$$$$$$|  $$$$$$|  $$$$$$ ")
    say ("| $$  | $$ /$$__  $$| $$ | $$ | $$| $$_____/| $$| $$_____/ \____  $$\____  $$")
    say ("| $$  | $$|  $$$$$$$| $$ | $$ | $$|  $$$$$$$| $$|  $$$$$$$ /$$$$$$$//$$$$$$$/")
    say ("|__/  |__/ \_______/|__/ |__/ |__/ \_______/|__/ \_______/|_______/|_______/ ")


while True :
    try:
        data = irc.recv ( 4096 ).decode ( "UTF-8" ).replace ( "\r\n", "" )
    except UnicodeDecodeError:
        print("This bot encountered a unicode decoding error.")
    print ( strftime ( "%H:%M:%S" ) + ' === ' + data)
    if data.split ( ' ' )[1] == 'NICK':
        botnick = data.split ( ' ' )[ 2 ].split ( ':' )[ 0 ]
    #THIS IF BLOCK DEALS WITH PINGS
    if data.split()[ 0 ] == "PING" :
        irc.send ( bytes ( 'PONG ' + data.split() [ 1 ] + '\r\n', "UTF-8" ) )
        print ( "PING SENT" )

    #PUT EVERYTHING THE BOT IS TO RESPOND TO (IN PUBLIC OR PRIVATE) WITHIN THIS BLOCK
    if data.split()[1] == "PRIVMSG" :
        #lets define some variables to make coding more easy and fun
        #making a bot do stuff really depends a lot on your ability
        #to parse the incoming data, interpret it, and respond.
        #read about split() and join(), and slicing for more infos.

        src_nick = data.split ( "!" )[ 0 ].replace ( ":", "" ) #nickname of person responsible for received text
        src_username = data.split ( " " )[ 0 ].split ( "@" )[ 0 ].split ( "!" )[ 1 ] #username of person responsible for recvd text
        src_hostname = data.split ( "@" )[ 1 ].split( " " )[ 0 ] #hostname of person responsible for recvd text (@irc2p)
        msg = ":".join ( data.split ( ":" )[ 2: ]).replace ( "\r\n", "" ) #receieved text from someone
        msg_dest = data.split ( " " )[ 2 ].split('!')[0]#channel or person message was determined for
        if len(msg.split()) != 0:
            commandword = msg.split()[ 0 ]
        else: commandword = "nothing"


        #WE CAN SEPARATE OUR PUBLIC AND PRIVATE COMMANDS LIKE SO
        #PUT PRIVATE COMMANDS YOU WANT THE BOT TO RESPOND TO IN THIS BLOCK
        print(msg_dest + ':' + botnick)
        if msg_dest == botnick :
            if msg == "hi" :
                irc.send ( bytes( 'PRIVMSG %s :hi there %s!\r\n'% ( src_nick, src_nick ), "UTF-8" ) )
            else: print("Something went wrong.")
            if msg.split ( ' ' )[ 0 ] == 'slap' and len ( msg.split( ' ' ) ) == 2:
                irc.send ( bytes( "PRIVMSG %s :\x01Action slaps %s!\x01\r\n"% ( botchannel, msg.split ( ' ' )[ 1 ] ), "UTF-8") )
            if msg.split ( ' ' )[ 0 ] == 'say' and len ( msg.split( ' ' ) ) >= 2:
                irc.send ( bytes( 'PRIVMSG %s :%s\r\n'% ( botchannel, msg.strip('say ')), "UTF-8") )
            
         

        #PUT PUBLIC COMMANDS YOU WANT THE BOT TO RESPOND TO IN THIS BLOCK
        elif msg_dest == botchannel:

            #HERE ARE EXAMPLE TEMPLATES OF HOW YOU WOULD CODE A RESPONSE TO SOMETHING SPECIFIC

            if "botty who are you" in msg :
                say ( 'I am botty, and I\'m you\'re worst nightmare')

            #In this example, as long as anywhere in the message is said "botty is stupid" it will reply.
            if "botty is stupid" in msg :
                say ( '%s: NO YOU\'RE STUPID!'% ( src_nick ) )

            #In this example, commandword is specifically looking at the first word in the message
            if commandword == "%sping"% ( bottrigger ) :
                say ( '%s: PONG!\r\n'% ( src_nick ) )
            if "slaps botty" in msg :
                say ( '%s: This is the Trout Protection Agency. Please put the Trout Down and walk away with your hands in the air.'% ( src_nick ) )
            if commandword == "%sinfo"% ( bottrigger ) :
                nameless()
                import sys
                say ( "Nameless-bot anti-copyright Schnaubelt 2012" )
                say ( "Python version: " + sys.version.replace( "\n", "" ) )
            if "cheese" in msg or "cheeze" in msg:
                if msg == "botty, do you like cheese?" :
                    say ( 'I love it more than life itself!' )
                else:
                    say ( 'WHERE!?!?!?!' )
#Prints nameless in 1337 ascii art.
            if commandword == "%snameless"% ( bottrigger ) :
                nameless()
            if commandword == "%sroll"% ( bottrigger ) :
                print(  msg.split ( ' ' ) )
                if len(  msg.split ( ' ' ) ) == 2:
                    try:
                        say ( src_nick + " rolled a dice with " + msg.split( ' ' )[ 1 ] + " sides and got a " + str( roll( int( msg.split( ' ' )[ 1 ] ) ) ) )
                    except ValueError:
                        say ( src_nick + " did not enter a valid number, thus no dice was rolled.")
                else:
                    say ( src_nick + " rolled a dice with 6 sides and got a " + str( roll(6 ) ) )
            if commandword == "%ssay"% ( bottrigger ) :
                say ( "This is a say test." )
            if commandword == "%sprint"% ( bottrigger ) :
                if len ( msg.split( ' ' ) ) == 2:
                    try:
                        say ( vars()[ msg.split ( ' ' )[ 1 ] ] )
                    except KeyError:
                        say ( msg.split ( ' ' )[ 1 ] + " is not a valid variable.")
                else: say ( "Please provide a variable." )

            if commandword == "%sdoc"% ( bottrigger ) :
                if len( msg.split ( ' ' ) ) == 2 :
                    arg = msg.split ( ' ' )[ 1 ]
                    if arg == 'roll' :
                        say ( bottrigger + 'roll simulates the roll of a die. Without parameters, it rolls a six sided die. Give it a number, and it will roll with a die with that many sides')
                    elif arg == 'nameless' : say ( "Prints NAMELESS in beautiful ascii art." )
                    elif arg == 'info' : say ( "Displays nameless ascii art along with anti-copyright and system information.")
                    elif arg == 'mushroom' : say ( "Prints an ascii-art one-up mushroom." )
                    elif arg == 'ping' : say ( "Use this, and your ping will be ponged. Often used to test lag." )
                    else : say ("I'm sorry. I have no documentation for " + arg + ".")
                else : say(bottrigger + 'doc is a basic documentation system for this bot\'s commands. To use it, simply type "' + bottrigger + 'doc <command>"')

#prints a colored irc 1-up mushroom.
            if commandword == "%smushroom"% (bottrigger) :
                say("0,1                                        ")
                say("0,1  0,3                                    0,1  ")
                say("0,1  0,3            0,1            0,3            0,1  ")
                say("0,1  0,3        0,1    0,4            0,1    0,3        0,1  ")
                say("0,1  0,3      0,1  0,4                    0,1  0,3      0,1  ")
                say("0,1  0,3    0,1  0,4  0,0    0,4                  0,1  0,3    0,1  ")
                say("0,1  0,3  0,1  0,4  0,0      0,4                    0,1  0,3  0,1  ")
                say("0,1  0,3  0,1  0,4    0,0    0,4    0,0  0,4  0,0  0,4  0,0    0,4    0,1  0,3  0,1  ")
                say("0,1  0,3  0,1  0,4    0,0    0,4    0,0  0,4  0,0  0,4  0,0  0,4  0,0  0,4  0,1  0,3  0,1  ")
                say("0,1  0,3  0,1  0,4    0,0    0,4    0,0  0,4  0,0  0,4  0,0    0,4    0,1  0,3  0,1  ")
                say("0,1  0,3  0,1  0,4  0,0        0,4    0,0  0,4    0,0  0,4      0,1  0,3  0,1  ")
                say("0,1  0,3  0,1    0,4                        0,1    0,3  0,1  ")
                say("0,1  0,3    0,1                            0,3    0,1  ")
                say("0,1  0,3          0,1  0,15            0,1  0,3          0,1  ")
                say("0,1  0,3        0,1  0,14  0,15  0,0        0,15  0,14  0,1  0,3        0,1  ")
                say("0,1  0,3        0,1  0,15    0,0        0,15    0,1  0,3        0,1  ")
                say("0,1  0,3        0,1    0,15            0,1    0,3        0,1  ")
                say("0,1  0,3          0,1                0,3          0,1  ")
                say("0,1  0,3                                    0,1  ")
                say("0,1  0,1  0,1  0,1  0,1  0,1  0,1  0,1  0,1  0,1  0,1  0,1  0,1  0,1  0,1  0,1  0,1 0,1 0,1 0,1 0,1 0,1 0,1 0,1 0,1")
